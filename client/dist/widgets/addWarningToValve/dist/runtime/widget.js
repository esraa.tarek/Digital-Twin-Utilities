System.register(["jimu-core/react","jimu-core","jimu-arcgis","esri/layers/GraphicsLayer","esri/Graphic","esri/geometry/SpatialReference"], function(__WEBPACK_DYNAMIC_EXPORT__, __system_context__) {
	var __WEBPACK_EXTERNAL_MODULE_react__ = {};
	var __WEBPACK_EXTERNAL_MODULE_jimu_core__ = {};
	var __WEBPACK_EXTERNAL_MODULE_jimu_arcgis__ = {};
	var __WEBPACK_EXTERNAL_MODULE_esri_layers_GraphicsLayer__ = {};
	var __WEBPACK_EXTERNAL_MODULE_esri_Graphic__ = {};
	var __WEBPACK_EXTERNAL_MODULE_esri_geometry_SpatialReference__ = {};
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_react__, "__esModule", { value: true });
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_jimu_core__, "__esModule", { value: true });
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_jimu_arcgis__, "__esModule", { value: true });
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_esri_layers_GraphicsLayer__, "__esModule", { value: true });
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_esri_Graphic__, "__esModule", { value: true });
	Object.defineProperty(__WEBPACK_EXTERNAL_MODULE_esri_geometry_SpatialReference__, "__esModule", { value: true });
	return {
		setters: [
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_react__[key] = module[key];
				});
			},
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_jimu_core__[key] = module[key];
				});
			},
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_jimu_arcgis__[key] = module[key];
				});
			},
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_esri_layers_GraphicsLayer__[key] = module[key];
				});
			},
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_esri_Graphic__[key] = module[key];
				});
			},
			function(module) {
				Object.keys(module).forEach(function(key) {
					__WEBPACK_EXTERNAL_MODULE_esri_geometry_SpatialReference__[key] = module[key];
				});
			}
		],
		execute: function() {
			__WEBPACK_DYNAMIC_EXPORT__(
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "esri/Graphic":
/*!*******************************!*\
  !*** external "esri/Graphic" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_esri_Graphic__;

/***/ }),

/***/ "esri/geometry/SpatialReference":
/*!*************************************************!*\
  !*** external "esri/geometry/SpatialReference" ***!
  \*************************************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_esri_geometry_SpatialReference__;

/***/ }),

/***/ "esri/layers/GraphicsLayer":
/*!********************************************!*\
  !*** external "esri/layers/GraphicsLayer" ***!
  \********************************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_esri_layers_GraphicsLayer__;

/***/ }),

/***/ "jimu-arcgis":
/*!******************************!*\
  !*** external "jimu-arcgis" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_jimu_arcgis__;

/***/ }),

/***/ "jimu-core":
/*!****************************!*\
  !*** external "jimu-core" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_jimu_core__;

/***/ }),

/***/ "react":
/*!**********************************!*\
  !*** external "jimu-core/react" ***!
  \**********************************/
/***/ ((module) => {

"use strict";
module.exports = __WEBPACK_EXTERNAL_MODULE_react__;

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		__webpack_require__.p = "";
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other entry modules.
(() => {
/*!******************************************!*\
  !*** ./jimu-core/lib/set-public-path.ts ***!
  \******************************************/
/**
 * Webpack will replace __webpack_public_path__ with __webpack_require__.p to set the public path dynamically.
 * The reason why we can't set the publicPath in webpack config is: we change the publicPath when download.
 * */
// eslint-disable-next-line
// @ts-ignore
__webpack_require__.p = window.jimuConfig.baseUrl;

})();

// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**************************************************************************!*\
  !*** ./your-extensions/widgets/addWarningToValve/src/runtime/widget.tsx ***!
  \**************************************************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var jimu_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jimu-core */ "jimu-core");
/* harmony import */ var jimu_arcgis__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jimu-arcgis */ "jimu-arcgis");
/* harmony import */ var esri_layers_GraphicsLayer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! esri/layers/GraphicsLayer */ "esri/layers/GraphicsLayer");
/* harmony import */ var esri_Graphic__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! esri/Graphic */ "esri/Graphic");
/* harmony import */ var esri_geometry_SpatialReference__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! esri/geometry/SpatialReference */ "esri/geometry/SpatialReference");






const Widget = (props) => {
    var _a;
    const [jimuMapView, setJimuMapView] = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)();
    let graphicsLayer;
    (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
        if (jimuMapView) {
            jimuMapView.view.when(() => {
                graphicsLayer = new esri_layers_GraphicsLayer__WEBPACK_IMPORTED_MODULE_3__["default"]({
                    "id": "Warning",
                    "title": "Warning",
                    "elevationInfo": { "mode": "relative-to-scene", "offset": -6 }
                });
                jimuMapView.view.map.add(graphicsLayer);
            });
        }
    }, [jimuMapView]);
    const activeViewChangeHandler = (jmv) => {
        if (jmv) {
            setJimuMapView(jmv);
        }
    };
    const onAddWarningIconClick = () => {
        console.log(jimuMapView);
        console.log(jimuMapView.view);
        // jimuMapView.view.layerViews._items[0].layerViews._items[1].layer
        jimuMapView.view.layerViews._items[2].layerViews._items[1].layer
            .queryFeatures({ "where": "OBJECTID_1=185", "returnGeometry": true })
            .then((data) => {
            console.log(data.features[0]);
            const point = {
                type: "point",
                x: data.features[0].geometry.rings[0][0][1],
                y: data.features[0].geometry.rings[0][0][0],
                z: 1010,
                spatialReference: esri_geometry_SpatialReference__WEBPACK_IMPORTED_MODULE_5__["default"].WebMercator
            };
            const markerSymbol = {
                type: "simple-marker",
                color: [255, 0, 0],
                outline: {
                    // autocasts as new SimpleLineSymbol()
                    color: [255, 0, 0],
                    width: 50
                },
                // verticalOffset: {
                //   screenLength: 40,
                //   maxWorldLength: 200,
                //   minWorldLength: 35
                // },
                // callout: {
                //   type: "line", // autocasts as new LineCallout3D()
                //   color: "white",
                //   size: 2,
                //   border: {
                //     color: [50, 50, 50]
                //   }
                // }
            };
            const pointGraphic = new esri_Graphic__WEBPACK_IMPORTED_MODULE_4__["default"]({
                geometry: data.features[0].geometry.centroid,
                symbol: markerSymbol
            });
            graphicsLayer.add(pointGraphic);
            // jimuMapView.view.goTo(jimuMapView.view.map.presentation.slides.getItemAt(0).viewpoint, {
            //   duration: 1000
            // }).catch((error) => {
            //   if (error.name !== 'AbortError') {
            //     console.error(error)
            //   }
            // })
            jimuMapView.view.goTo({
                center: data.features[0].geometry.centroid,
                zoom: 25
            });
            // data.features[0].geometry.rings[0][0] //first point in the manhole geometry
        })
            .catch((err) => {
            console.log(err);
        });
    };
    return (jimu_core__WEBPACK_IMPORTED_MODULE_1__.React.createElement("div", { className: "widget-demo jimu-widget m-2" },
        props.useMapWidgetIds &&
            props.useMapWidgetIds.length === 1 && (jimu_core__WEBPACK_IMPORTED_MODULE_1__.React.createElement(jimu_arcgis__WEBPACK_IMPORTED_MODULE_2__.JimuMapViewComponent, { useMapWidgetId: (_a = props.useMapWidgetIds) === null || _a === void 0 ? void 0 : _a[0], onActiveViewChange: activeViewChangeHandler })),
        jimu_core__WEBPACK_IMPORTED_MODULE_1__.React.createElement("button", { onClick: onAddWarningIconClick }, "Add Warning to Valve")));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Widget);

})();

/******/ 	return __webpack_exports__;
/******/ })()

			);
		}
	};
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0cy9hZGRXYXJuaW5nVG9WYWx2ZS9kaXN0L3J1bnRpbWUvd2lkZ2V0LmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7O1VDQUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7V0N0QkE7V0FDQTtXQUNBO1dBQ0E7V0FDQSx5Q0FBeUMsd0NBQXdDO1dBQ2pGO1dBQ0E7V0FDQTs7Ozs7V0NQQTs7Ozs7V0NBQTtXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7O1dDTkE7Ozs7Ozs7Ozs7QUNBQTs7O0tBR0s7QUFDTCwyQkFBMkI7QUFDM0IsYUFBYTtBQUNiLHFCQUF1QixHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsT0FBTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOUjtBQUVNO0FBQ2M7QUFFVjtBQUNuQjtBQUMyQjtBQUk3RCxNQUFNLE1BQU0sR0FBRyxDQUFDLEtBQStCLEVBQUUsRUFBRTs7SUFDakQsTUFBTSxDQUFDLFdBQVcsRUFBRSxjQUFjLENBQUMsR0FBRywrQ0FBUSxFQUFlO0lBRTdELElBQUksYUFBYSxDQUFDO0lBRWxCLGdEQUFTLENBQUMsR0FBRSxFQUFFO1FBQ1osSUFBRyxXQUFXLEVBQUM7WUFDYixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBQ3pCLGFBQWEsR0FBRyxJQUFJLGlFQUFhLENBQy9CO29CQUNFLElBQUksRUFBRSxTQUFTO29CQUNmLE9BQU8sRUFBRSxTQUFTO29CQUNsQixlQUFlLEVBQUUsRUFBQyxNQUFNLEVBQUUsbUJBQW1CLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFDO2lCQUM3RCxDQUNBLENBQUM7Z0JBQ0osV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzFDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDLEVBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUVoQixNQUFNLHVCQUF1QixHQUFHLENBQUMsR0FBZ0IsRUFBRSxFQUFFO1FBQ25ELElBQUksR0FBRyxFQUFFO1lBQ1AsY0FBYyxDQUFDLEdBQUcsQ0FBQztTQUNwQjtJQUNILENBQUM7SUFFRCxNQUFNLHFCQUFxQixHQUFHLEdBQUcsRUFBRTtRQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQztRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUU5QixtRUFBbUU7UUFDbkUsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSzthQUMvRCxhQUFhLENBQUMsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsSUFBSSxFQUFDLENBQUM7YUFDbEUsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFDLEVBQUU7WUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLEtBQUssR0FBRztnQkFDWixJQUFJLEVBQUUsT0FBTztnQkFDYixDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLENBQUMsRUFBRSxJQUFJO2dCQUNQLGdCQUFnQixFQUFFLGtGQUE0QjthQUMvQyxDQUFDO1lBRUYsTUFBTSxZQUFZLEdBQUc7Z0JBQ25CLElBQUksRUFBRSxlQUFlO2dCQUNyQixLQUFLLEVBQUUsQ0FBQyxHQUFHLEVBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQztnQkFDaEIsT0FBTyxFQUFFO29CQUNQLHNDQUFzQztvQkFDdEMsS0FBSyxFQUFFLENBQUMsR0FBRyxFQUFDLENBQUMsRUFBQyxDQUFDLENBQUM7b0JBQ2hCLEtBQUssRUFBRSxFQUFFO2lCQUNWO2dCQUNELG9CQUFvQjtnQkFDcEIsc0JBQXNCO2dCQUN0Qix5QkFBeUI7Z0JBQ3pCLHVCQUF1QjtnQkFDdkIsS0FBSztnQkFDTCxhQUFhO2dCQUNiLHNEQUFzRDtnQkFDdEQsb0JBQW9CO2dCQUNwQixhQUFhO2dCQUNiLGNBQWM7Z0JBQ2QsMEJBQTBCO2dCQUMxQixNQUFNO2dCQUNOLElBQUk7YUFDTCxDQUFDO1lBRUYsTUFBTSxZQUFZLEdBQUcsSUFBSSxvREFBTyxDQUFDO2dCQUMvQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUTtnQkFDNUMsTUFBTSxFQUFFLFlBQVk7YUFDckIsQ0FBQyxDQUFDO1lBRUgsYUFBYSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUVoQywyRkFBMkY7WUFDM0YsbUJBQW1CO1lBQ25CLHdCQUF3QjtZQUN4Qix1Q0FBdUM7WUFDdkMsMkJBQTJCO1lBQzNCLE1BQU07WUFDTixLQUFLO1lBRUwsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3BCLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRO2dCQUMxQyxJQUFJLEVBQUUsRUFBRTthQUNULENBQUMsQ0FBQztZQUNILDhFQUE4RTtRQUNoRixDQUFDLENBQUM7YUFDRCxLQUFLLENBQUMsQ0FBQyxHQUFHLEVBQUMsRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELE9BQU8sQ0FDTCxvRUFBSyxTQUFTLEVBQUMsNkJBQTZCO1FBRXhDLEtBQUssQ0FBQyxlQUFlO1lBQ3JCLEtBQUssQ0FBQyxlQUFlLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUNwQywyREFBQyw2REFBb0IsSUFDbkIsY0FBYyxFQUFFLFdBQUssQ0FBQyxlQUFlLDBDQUFHLENBQUMsQ0FBQyxFQUMxQyxrQkFBa0IsRUFBRSx1QkFBdUIsR0FDM0MsQ0FDSDtRQUVILHVFQUFRLE9BQU8sRUFBRSxxQkFBcUIsMkJBQStCLENBQ2pFLENBQ1A7QUFDSCxDQUFDO0FBRUQsaUVBQWUsTUFBTSIsInNvdXJjZXMiOlsid2VicGFjazovL2V4Yi1jbGllbnQvZXh0ZXJuYWwgc3lzdGVtIFwiZXNyaS9HcmFwaGljXCIiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC9leHRlcm5hbCBzeXN0ZW0gXCJlc3JpL2dlb21ldHJ5L1NwYXRpYWxSZWZlcmVuY2VcIiIsIndlYnBhY2s6Ly9leGItY2xpZW50L2V4dGVybmFsIHN5c3RlbSBcImVzcmkvbGF5ZXJzL0dyYXBoaWNzTGF5ZXJcIiIsIndlYnBhY2s6Ly9leGItY2xpZW50L2V4dGVybmFsIHN5c3RlbSBcImppbXUtYXJjZ2lzXCIiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC9leHRlcm5hbCBzeXN0ZW0gXCJqaW11LWNvcmVcIiIsIndlYnBhY2s6Ly9leGItY2xpZW50L2V4dGVybmFsIHN5c3RlbSBcImppbXUtY29yZS9yZWFjdFwiIiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vZXhiLWNsaWVudC93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovL2V4Yi1jbGllbnQvd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly9leGItY2xpZW50L3dlYnBhY2svcnVudGltZS9wdWJsaWNQYXRoIiwid2VicGFjazovL2V4Yi1jbGllbnQvLi9qaW11LWNvcmUvbGliL3NldC1wdWJsaWMtcGF0aC50cyIsIndlYnBhY2s6Ly9leGItY2xpZW50Ly4veW91ci1leHRlbnNpb25zL3dpZGdldHMvYWRkV2FybmluZ1RvVmFsdmUvc3JjL3J1bnRpbWUvd2lkZ2V0LnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfZXNyaV9HcmFwaGljX187IiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX2VzcmlfZ2VvbWV0cnlfU3BhdGlhbFJlZmVyZW5jZV9fOyIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV9lc3JpX2xheWVyc19HcmFwaGljc0xheWVyX187IiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX2ppbXVfYXJjZ2lzX187IiwibW9kdWxlLmV4cG9ydHMgPSBfX1dFQlBBQ0tfRVhURVJOQUxfTU9EVUxFX2ppbXVfY29yZV9fOyIsIm1vZHVsZS5leHBvcnRzID0gX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV9yZWFjdF9fOyIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9ucyBmb3IgaGFybW9ueSBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSAoZXhwb3J0cywgZGVmaW5pdGlvbikgPT4ge1xuXHRmb3IodmFyIGtleSBpbiBkZWZpbml0aW9uKSB7XG5cdFx0aWYoX193ZWJwYWNrX3JlcXVpcmVfXy5vKGRlZmluaXRpb24sIGtleSkgJiYgIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBrZXkpKSB7XG5cdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywga2V5LCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZGVmaW5pdGlvbltrZXldIH0pO1xuXHRcdH1cblx0fVxufTsiLCJfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSAob2JqLCBwcm9wKSA9PiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgcHJvcCkpIiwiLy8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuX193ZWJwYWNrX3JlcXVpcmVfXy5yID0gKGV4cG9ydHMpID0+IHtcblx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG5cdH1cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbn07IiwiX193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjsiLCIvKipcclxuICogV2VicGFjayB3aWxsIHJlcGxhY2UgX193ZWJwYWNrX3B1YmxpY19wYXRoX18gd2l0aCBfX3dlYnBhY2tfcmVxdWlyZV9fLnAgdG8gc2V0IHRoZSBwdWJsaWMgcGF0aCBkeW5hbWljYWxseS5cclxuICogVGhlIHJlYXNvbiB3aHkgd2UgY2FuJ3Qgc2V0IHRoZSBwdWJsaWNQYXRoIGluIHdlYnBhY2sgY29uZmlnIGlzOiB3ZSBjaGFuZ2UgdGhlIHB1YmxpY1BhdGggd2hlbiBkb3dubG9hZC5cclxuICogKi9cclxuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lXHJcbi8vIEB0cy1pZ25vcmVcclxuX193ZWJwYWNrX3B1YmxpY19wYXRoX18gPSB3aW5kb3cuamltdUNvbmZpZy5iYXNlVXJsXHJcbiIsImltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tICdyZWFjdCdcclxuXHJcbmltcG9ydCB7IFJlYWN0LCBBbGxXaWRnZXRQcm9wcyB9IGZyb20gJ2ppbXUtY29yZSdcclxuaW1wb3J0IHsgSmltdU1hcFZpZXdDb21wb25lbnQsIEppbXVNYXBWaWV3IH0gZnJvbSAnamltdS1hcmNnaXMnXHJcblxyXG5pbXBvcnQgR3JhcGhpY3NMYXllciBmcm9tICdlc3JpL2xheWVycy9HcmFwaGljc0xheWVyJ1xyXG5pbXBvcnQgR3JhcGhpYyBmcm9tICdlc3JpL0dyYXBoaWMnXHJcbmltcG9ydCBTcGF0aWFsUmVmZXJlbmNlIGZyb20gXCJlc3JpL2dlb21ldHJ5L1NwYXRpYWxSZWZlcmVuY2VcIlxyXG5cclxuaW1wb3J0IHsgSU1Db25maWcgfSBmcm9tICcuLi9jb25maWcnXHJcblxyXG5jb25zdCBXaWRnZXQgPSAocHJvcHM6IEFsbFdpZGdldFByb3BzPElNQ29uZmlnPikgPT4ge1xyXG4gIGNvbnN0IFtqaW11TWFwVmlldywgc2V0SmltdU1hcFZpZXddID0gdXNlU3RhdGU8SmltdU1hcFZpZXc+KClcclxuXHJcbiAgbGV0IGdyYXBoaWNzTGF5ZXI7XHJcblxyXG4gIHVzZUVmZmVjdCgoKT0+e1xyXG4gICAgaWYoamltdU1hcFZpZXcpe1xyXG4gICAgICBqaW11TWFwVmlldy52aWV3LndoZW4oKCkgPT4ge1xyXG4gICAgICAgIGdyYXBoaWNzTGF5ZXIgPSBuZXcgR3JhcGhpY3NMYXllcihcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgXCJpZFwiOiBcIldhcm5pbmdcIixcclxuICAgICAgICAgICAgXCJ0aXRsZVwiOiBcIldhcm5pbmdcIixcclxuICAgICAgICAgICAgXCJlbGV2YXRpb25JbmZvXCI6IHtcIm1vZGVcIjogXCJyZWxhdGl2ZS10by1zY2VuZVwiLCBcIm9mZnNldFwiOiAtNn1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgamltdU1hcFZpZXcudmlldy5tYXAuYWRkKGdyYXBoaWNzTGF5ZXIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9LFtqaW11TWFwVmlld10pXHJcblxyXG4gIGNvbnN0IGFjdGl2ZVZpZXdDaGFuZ2VIYW5kbGVyID0gKGptdjogSmltdU1hcFZpZXcpID0+IHtcclxuICAgIGlmIChqbXYpIHtcclxuICAgICAgc2V0SmltdU1hcFZpZXcoam12KVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3Qgb25BZGRXYXJuaW5nSWNvbkNsaWNrID0gKCkgPT57XHJcbiAgICBjb25zb2xlLmxvZyhqaW11TWFwVmlldylcclxuICAgIGNvbnNvbGUubG9nKGppbXVNYXBWaWV3LnZpZXcpO1xyXG5cclxuICAgIC8vIGppbXVNYXBWaWV3LnZpZXcubGF5ZXJWaWV3cy5faXRlbXNbMF0ubGF5ZXJWaWV3cy5faXRlbXNbMV0ubGF5ZXJcclxuICAgIGppbXVNYXBWaWV3LnZpZXcubGF5ZXJWaWV3cy5faXRlbXNbMl0ubGF5ZXJWaWV3cy5faXRlbXNbMV0ubGF5ZXJcclxuICAgIC5xdWVyeUZlYXR1cmVzKHtcIndoZXJlXCI6IFwiT0JKRUNUSURfMT0xODVcIiwgXCJyZXR1cm5HZW9tZXRyeVwiOiB0cnVlfSlcclxuICAgIC50aGVuKChkYXRhKT0+e1xyXG4gICAgICBjb25zb2xlLmxvZyhkYXRhLmZlYXR1cmVzWzBdKTtcclxuICAgICAgY29uc3QgcG9pbnQgPSB7XHJcbiAgICAgICAgdHlwZTogXCJwb2ludFwiLCAvLyBhdXRvY2FzdHMgYXMgbmV3IFBvaW50KClcclxuICAgICAgICB4OiBkYXRhLmZlYXR1cmVzWzBdLmdlb21ldHJ5LnJpbmdzWzBdWzBdWzFdLFxyXG4gICAgICAgIHk6IGRhdGEuZmVhdHVyZXNbMF0uZ2VvbWV0cnkucmluZ3NbMF1bMF1bMF0sXHJcbiAgICAgICAgejogMTAxMCxcclxuICAgICAgICBzcGF0aWFsUmVmZXJlbmNlOiBTcGF0aWFsUmVmZXJlbmNlLldlYk1lcmNhdG9yXHJcbiAgICAgIH07XHJcblxyXG4gICAgICBjb25zdCBtYXJrZXJTeW1ib2wgPSB7XHJcbiAgICAgICAgdHlwZTogXCJzaW1wbGUtbWFya2VyXCIsIC8vIGF1dG9jYXN0cyBhcyBuZXcgU2ltcGxlTWFya2VyU3ltYm9sKClcclxuICAgICAgICBjb2xvcjogWzI1NSwwLDBdLFxyXG4gICAgICAgIG91dGxpbmU6IHtcclxuICAgICAgICAgIC8vIGF1dG9jYXN0cyBhcyBuZXcgU2ltcGxlTGluZVN5bWJvbCgpXHJcbiAgICAgICAgICBjb2xvcjogWzI1NSwwLDBdLFxyXG4gICAgICAgICAgd2lkdGg6IDUwXHJcbiAgICAgICAgfSxcclxuICAgICAgICAvLyB2ZXJ0aWNhbE9mZnNldDoge1xyXG4gICAgICAgIC8vICAgc2NyZWVuTGVuZ3RoOiA0MCxcclxuICAgICAgICAvLyAgIG1heFdvcmxkTGVuZ3RoOiAyMDAsXHJcbiAgICAgICAgLy8gICBtaW5Xb3JsZExlbmd0aDogMzVcclxuICAgICAgICAvLyB9LFxyXG4gICAgICAgIC8vIGNhbGxvdXQ6IHtcclxuICAgICAgICAvLyAgIHR5cGU6IFwibGluZVwiLCAvLyBhdXRvY2FzdHMgYXMgbmV3IExpbmVDYWxsb3V0M0QoKVxyXG4gICAgICAgIC8vICAgY29sb3I6IFwid2hpdGVcIixcclxuICAgICAgICAvLyAgIHNpemU6IDIsXHJcbiAgICAgICAgLy8gICBib3JkZXI6IHtcclxuICAgICAgICAvLyAgICAgY29sb3I6IFs1MCwgNTAsIDUwXVxyXG4gICAgICAgIC8vICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIGNvbnN0IHBvaW50R3JhcGhpYyA9IG5ldyBHcmFwaGljKHtcclxuICAgICAgICBnZW9tZXRyeTogZGF0YS5mZWF0dXJlc1swXS5nZW9tZXRyeS5jZW50cm9pZCxcclxuICAgICAgICBzeW1ib2w6IG1hcmtlclN5bWJvbFxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIGdyYXBoaWNzTGF5ZXIuYWRkKHBvaW50R3JhcGhpYyk7XHJcblxyXG4gICAgICAvLyBqaW11TWFwVmlldy52aWV3LmdvVG8oamltdU1hcFZpZXcudmlldy5tYXAucHJlc2VudGF0aW9uLnNsaWRlcy5nZXRJdGVtQXQoMCkudmlld3BvaW50LCB7XHJcbiAgICAgIC8vICAgZHVyYXRpb246IDEwMDBcclxuICAgICAgLy8gfSkuY2F0Y2goKGVycm9yKSA9PiB7XHJcbiAgICAgIC8vICAgaWYgKGVycm9yLm5hbWUgIT09ICdBYm9ydEVycm9yJykge1xyXG4gICAgICAvLyAgICAgY29uc29sZS5lcnJvcihlcnJvcilcclxuICAgICAgLy8gICB9XHJcbiAgICAgIC8vIH0pXHJcblxyXG4gICAgICBqaW11TWFwVmlldy52aWV3LmdvVG8oe1xyXG4gICAgICAgIGNlbnRlcjogZGF0YS5mZWF0dXJlc1swXS5nZW9tZXRyeS5jZW50cm9pZCxcclxuICAgICAgICB6b29tOiAyNVxyXG4gICAgICB9KTtcclxuICAgICAgLy8gZGF0YS5mZWF0dXJlc1swXS5nZW9tZXRyeS5yaW5nc1swXVswXSAvL2ZpcnN0IHBvaW50IGluIHRoZSBtYW5ob2xlIGdlb21ldHJ5XHJcbiAgICB9KVxyXG4gICAgLmNhdGNoKChlcnIpPT57XHJcbiAgICAgIGNvbnNvbGUubG9nKGVycik7XHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPVwid2lkZ2V0LWRlbW8gamltdS13aWRnZXQgbS0yXCI+XHJcbiAgICAgIHtcclxuICAgICAgICBwcm9wcy51c2VNYXBXaWRnZXRJZHMgJiZcclxuICAgICAgICBwcm9wcy51c2VNYXBXaWRnZXRJZHMubGVuZ3RoID09PSAxICYmIChcclxuICAgICAgICAgIDxKaW11TWFwVmlld0NvbXBvbmVudFxyXG4gICAgICAgICAgICB1c2VNYXBXaWRnZXRJZD17cHJvcHMudXNlTWFwV2lkZ2V0SWRzPy5bMF19XHJcbiAgICAgICAgICAgIG9uQWN0aXZlVmlld0NoYW5nZT17YWN0aXZlVmlld0NoYW5nZUhhbmRsZXJ9XHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgIClcclxuICAgICAgfVxyXG4gICAgICA8YnV0dG9uIG9uQ2xpY2s9e29uQWRkV2FybmluZ0ljb25DbGlja30+QWRkIFdhcm5pbmcgdG8gVmFsdmU8L2J1dHRvbj5cclxuICAgIDwvZGl2PlxyXG4gIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgV2lkZ2V0XHJcbiJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==