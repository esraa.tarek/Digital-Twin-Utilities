import { useState, useEffect } from 'react'

import { React, AllWidgetProps } from 'jimu-core'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis'

import GraphicsLayer from 'esri/layers/GraphicsLayer'
import Graphic from 'esri/Graphic'
import SpatialReference from "esri/geometry/SpatialReference"

import { IMConfig } from '../config'

const Widget = (props: AllWidgetProps<IMConfig>) => {
  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()

  let graphicsLayer;

  useEffect(()=>{
    if(jimuMapView){
      jimuMapView.view.when(() => {
        graphicsLayer = new GraphicsLayer(
          {
            "id": "Warning",
            "title": "Warning",
            "elevationInfo": {"mode": "relative-to-scene", "offset": -6}
          }
          );
        jimuMapView.view.map.add(graphicsLayer);
      });
    }
  },[jimuMapView])

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }

  const onAddWarningIconClick = () =>{
    console.log(jimuMapView)
    console.log(jimuMapView.view);

    // jimuMapView.view.layerViews._items[0].layerViews._items[1].layer
    jimuMapView.view.layerViews._items[2].layerViews._items[1].layer
    .queryFeatures({"where": "OBJECTID_1=185", "returnGeometry": true})
    .then((data)=>{
      console.log(data.features[0]);
      const point = {
        type: "point", // autocasts as new Point()
        x: data.features[0].geometry.rings[0][0][1],
        y: data.features[0].geometry.rings[0][0][0],
        z: 1010,
        spatialReference: SpatialReference.WebMercator
      };

      const markerSymbol = {
        type: "simple-marker", // autocasts as new SimpleMarkerSymbol()
        color: [255,0,0],
        outline: {
          // autocasts as new SimpleLineSymbol()
          color: [255,0,0],
          width: 50
        },
        // verticalOffset: {
        //   screenLength: 40,
        //   maxWorldLength: 200,
        //   minWorldLength: 35
        // },
        // callout: {
        //   type: "line", // autocasts as new LineCallout3D()
        //   color: "white",
        //   size: 2,
        //   border: {
        //     color: [50, 50, 50]
        //   }
        // }
      };

      const pointGraphic = new Graphic({
        geometry: data.features[0].geometry.centroid,
        symbol: markerSymbol
      });

      graphicsLayer.add(pointGraphic);

      // jimuMapView.view.goTo(jimuMapView.view.map.presentation.slides.getItemAt(0).viewpoint, {
      //   duration: 1000
      // }).catch((error) => {
      //   if (error.name !== 'AbortError') {
      //     console.error(error)
      //   }
      // })

      jimuMapView.view.goTo({
        center: data.features[0].geometry.centroid,
        zoom: 25
      });
      // data.features[0].geometry.rings[0][0] //first point in the manhole geometry
    })
    .catch((err)=>{
      console.log(err);
    })
  }

  return (
    <div className="widget-demo jimu-widget m-2">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <button onClick={onAddWarningIconClick}>Add Warning to Valve</button>
    </div>
  )
}

export default Widget
