import { useState } from 'react'
import { React, AllWidgetProps } from 'jimu-core'
import { AllWidgetSettingProps } from 'jimu-for-builder'
import { JimuMapViewComponent, JimuMapView } from 'jimu-arcgis'
import { IMConfig } from '../config'
import { Slider } from 'jimu-ui'

const Widget = (props: AllWidgetProps<IMConfig>, settingsProps: AllWidgetSettingProps<any>) => {
  const [jimuMapView, setJimuMapView] = useState<JimuMapView>()

  const activeViewChangeHandler = (jmv: JimuMapView) => {
    if (jmv) {
      setJimuMapView(jmv)
    }
  }
  const onOpacityChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    // console.log(e.target.value)
    jimuMapView.view.map.basemap.baseLayers.items[0].opacity = e.target.value
  }
  const paddingValue = { paddingRight: '1em' }
  return (
    <div className="widget-demo jimu-widget m-2">
      {
        props.useMapWidgetIds &&
        props.useMapWidgetIds.length === 1 && (
          <JimuMapViewComponent
            useMapWidgetId={props.useMapWidgetIds?.[0]}
            onActiveViewChange={activeViewChangeHandler}
          />
        )
      }
      <div style={paddingValue}>
        <p>Change Basemap Opacity</p>
        <div>
          <Slider
          aria-label="Range"
          defaultValue={1}
          max={1}
          min={0}
          onChange={onOpacityChange}
          step={0.1}
        />
        </div>
      </div>
    </div>
  )
}

export default Widget
